use crate::utils;

use clap::ArgMatches;
use std::process::exit;

pub async fn search(arg_m: &ArgMatches) {
    let query = arg_m
        .values_of("query")
        .unwrap_or_default()
        .collect::<Vec<_>>()
        .join(" ");
    let query = query.trim();
    let results = utils::search(utils::create_client(), query).await.unwrap();
    if results.is_empty() {
        eprintln!("No results found");
        exit(1);
    }
    for i in results {
        println!("{}", i);
    }
}
