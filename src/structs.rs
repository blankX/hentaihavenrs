use serde::Deserialize;
use std::fmt;
extern crate quick_xml;
extern crate reqwest;
extern crate serde_json;

#[derive(Deserialize, Debug)]
pub struct SearchResult {
    pub id: i32,
    pub title: RenderedTitle,
}

#[derive(Deserialize, Debug)]
pub struct RenderedTitle {
    pub rendered: String,
}

impl fmt::Display for SearchResult {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str(&format!("{}: {}", self.id, &self.title.rendered))
    }
}

#[derive(Debug)]
pub struct HentaiInfo {
    pub id: String,
    pub slug: String,
    pub title: String,
    pub views: usize,
    pub genres: Vec<String>,
    pub censored: bool,
    pub episode_urls: Vec<String>,
    pub summary: String,
}

impl fmt::Display for HentaiInfo {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            formatter,
            "ID: {}\nTitle: {}\nViews: {}\nCensored: {}\nGenres: {}\nEpisodes: {}\nSummary:\n{}",
            &self.id,
            &self.title,
            self.views,
            match self.censored {
                true => "Yes",
                false => "No",
            },
            &self.genres.join(", "),
            self.episode_urls.len(),
            &self.summary
        )
    }
}

#[derive(Debug)]
pub struct HentaiVideo {
    pub captions: Option<String>,
    pub video: String,
}

impl fmt::Display for HentaiVideo {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut text = self.video.clone();
        if let Some(subtitles) = &self.captions {
            text.push(';');
            text.push_str(&subtitles);
        }
        formatter.write_str(&text)
    }
}

#[derive(Deserialize, Debug)]
pub struct RawHentaiVideo {
    pub data: RawHentaiVideoData,
}

#[derive(Deserialize, Debug)]
pub struct RawHentaiVideoData {
    pub captions: RawHentaiVideoSrc,
    pub sources: Vec<RawHentaiVideoSrc>,
}

#[derive(Deserialize, Debug)]
pub struct RawHentaiVideoSrc {
    pub src: String,
}

#[derive(Debug)]
pub enum Error {
    Reqwest(reqwest::Error),
    QuickXML(quick_xml::Error),
    SerdeJSON(serde_json::Error),
}

impl From<reqwest::Error> for Error {
    #[inline]
    fn from(error: reqwest::Error) -> Error {
        Error::Reqwest(error)
    }
}

impl From<quick_xml::Error> for Error {
    #[inline]
    fn from(error: quick_xml::Error) -> Error {
        Error::QuickXML(error)
    }
}

impl From<serde_json::Error> for Error {
    #[inline]
    fn from(error: serde_json::Error) -> Error {
        Error::SerdeJSON(error)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.write_str(&match self {
            Error::Reqwest(err) => format!("reqwest error: {}", err),
            Error::QuickXML(err) => format!("quick-xml error: {}", err),
            Error::SerdeJSON(err) => format!("serde_json error: {}", err),
        })
    }
}
